g_PluginInfo =
{
	Name = "keepInventory",
	Date = "2018-08-22",
	Description = [[
keepInventory is a plugin for cuberite that implents minecraft's keepInventory gamerule into cuberite.
		]],
	SourceLocation = "https://gitlab.com/d3nw0/keepInventory",

	-- The following members will be documented in greater detail later:
	Commands = {
		["/keepinventory"] = {
			HelpString = "keepInventory main command",
			Permission = "keepInventory.admin",
			Handler = CMDki,
			Subcommands = {
				["disable"] = {
					HelpString = "Disables the functionality of this plugin",
					Permission = "keepInventory.admin.disable",
					Handler = CMDkid,
				},
				["enable"] = {
					HelpString = "Enables the functionality of this plugin",
					Permission = "keepInventory.admin.enable",
					Handler = CMDkie,
				},
				["trigger"] = {
					HelpString = "Triggers the functionality of this plugin",
					Permission = "keepInventory.admin.trigger",
					Handler = CMDkit,
				},
			},
		},
	},
	ConsoleCommands = {
		keepinventory = {
			Subcommands = {
				disable = {
					HelpString = "Triggers the functionality of this plugin",
					Handler = CMDkid,
				},
				enable = {
					HelpString = "Triggers the functionality of this plugin",
					Handler = CMDkie,
				},
				trigger = {
					HelpString = "Triggers the functionality of this plugin",
					Handler = CMDkit,
				},
			},
		},
	},
}

