# keepInventory

keepInventory is a plugin for the extensible Minecraft server Cuberite, it saves a player's items upon death and returns it upon respawn.

Features:
- Ease of use

Roadmap:
- [x] Commands
- [x] Permissions

### Commands:
- keepinventory
    * disable
    * enable
    * trigger

### Permissions:
- keepinventory.admin
    * keepinventory.admin.disable
    * keepinventory.admin.enable
    * keepinventory.admin.trigger