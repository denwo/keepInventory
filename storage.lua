-- https://github.com/cuberite/Coiny
-- Storage.lua

-- Implements the storage for the player data and the functions for accessing it





-- The storage instance that provides the access and context to all the functions
g_Storage = {}





--- Stores the items in the database
-- Returns true on success, nil and error message on failure
function g_Storage:addItems(a_AccountName, a_AccountItems)
	-- Check the params:
	assert(type(a_AccountName) == "string")
	assert(type(a_AccountItems) == "string")
	
	-- Add the transaction to log:
	return self.DB:executeStatement(
		"INSERT INTO storedItems (AccountName, AccountItems, time) VALUES (?, ?, ?)",
		{ a_AccountName, a_AccountItems, os.time()}
	)
end





--- Returns the current items of the specified account
-- Returns nil and error message on failure
function g_Storage:getItems(a_AccountName)
	-- Check the params:
	assert(type(a_AccountName) == "string")

	-- Add all the incoming and outgoing transactions together to get the balance:
	local items = ""
	local isSuccess, errMsg = self.DB:executeStatement(
		"SELECT AccountItems AS a FROM storedItems WHERE AccountName = ?",
		{ a_AccountName },
		function (a_Values)
			-- If the query condition returns zero rows, then no value is returned in the resulting table
			items = a_Values["a"] or ""
		end
	)
	if not(isSuccess) then
		return nil, errMsg
	end

	-- Successfully queried the balance, return it:
	return items
end





--- Removes the specified amount to the specified account, with the specified message and channel.
-- Returns true on success, nil and error message on failure
function g_Storage:removeItems(a_AccountName)
	-- Check the params:
	assert(type(a_AccountName) == "string")
	
	-- Add the transaction to log:
	return self.DB:executeStatement(
		"DELETE FROM `storedItems` WHERE `AccountName` = ?;",
		{ a_AccountName}
	)
end

function InitializeStorage()
	-- Open the database:
	local ErrMsg
	g_Storage.DB, ErrMsg = newSQLiteDB("db.sqlite3")
	if not(g_Storage) then
		LOGWARNING("Cannot open the keepInventory database, disabling the plugin")
		error(ErrMsg)
	end
	
	-- Define the needed structure:
	local storedItemsColumns =
	{
		"AccountName",
		"AccountItems",
		"time",
	}
	
	-- Check / create structure:
	if not(g_Storage.DB:createDBTable("storedItems", storedItemsColumns)) then
		LOGWARNING("Cannot initialize the keepInventory database, disabling the plugin.")
		error("keepInventory Database failure")
	end
end

