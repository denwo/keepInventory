gEnabled = true
PLUGIN = nil

function Initialize(Plugin)
	Plugin:SetName("keepInventory")
	Plugin:SetVersion(1.0)

	PLUGIN = Plugin

	-- Hooks
	cPluginManager:AddHook(cPluginManager.HOOK_KILLING, BeforeKilled)
	cPluginManager:AddHook(cPluginManager.HOOK_PLAYER_SPAWNED, AfterRespawn)
	
	dofile(cPluginManager:GetPluginsPath() .. "/InfoReg.lua")
	RegisterPluginInfoCommands()
	RegisterPluginInfoConsoleCommands()

	InitializeStorage()

	LOG("Initialised " .. Plugin:GetName() .. " v." .. Plugin:GetVersion())

	return true
end

function BeforeKilled(victim, killer, tdi)
	if victim:IsPlayer() and gEnabled then
		victim.oldItems = cItems()
		local inv = victim:GetInventory()
	    	inv:CopyToItems(victim.oldItems)
	    	-- Incase the player disconnects
		g_Storage:addItems(victim:GetName(), GetStringByItems(victim.oldItems))
    		inv:Clear()
	end

	return false
end 

function AfterRespawn(l_Player)
	-- Check for items stored in the memory
	if l_Player then
		if l_Player.oldItems and l_Player.oldItems:Size() > 0 then
			l_Player:GetInventory():AddItems(l_Player.oldItems, true)
			-- Empty out the old items
			l_Player.oldItems = nil
		else
			local l_Items = GetItemsByString(g_Storage:getItems(l_Player:GetName()))
			l_Player:GetInventory():AddItems(l_Items, true)
		end
		-- Delete the stored items in the database
		g_Storage:removeItems(l_Player:GetName())
	end

	return false
end

function GetItemsByString(l_Str)

	assert(type(l_Str) == "string")

	local l_Items = cItems()
	local l_Split = l_Str:Split(",")

	for k, v in pairs(l_Split) do
		if v and v:len() > 0 then
			l_Items:Add(GetItemByString(v))
		end
	end

	return l_Items
end

function GetItemByString(l_Str)
	local l_Split = l_Str:Split(":")

	local l_Item
	if l_Split[1]:len() > 0 and l_Split[1] then
		l_Item = cItem(tonumber(l_Split[1]), (tonumber(l_Split[2]) or 0), (tonumber(l_Split[3]) or 0))

		l_Item.m_Enchantments = cEnchantments(l_Split[4])
		if l_Split[5] then
			l_Item.m_CustomName = l_Split[5]
		end
	end

	return l_Item
end

-- We cannot use ';', ':' and ',' cause the enchantment 
function GetStringByItems(l_Items)

	if l_Items == nil then return "" end

	local l_Str = ""

	for i=0, l_Items:Size()-1 do
		local l_Item = l_Items:Get(i)
		l_Str = l_Str .. AddToString(l_Item.m_ItemType, l_Item.m_ItemCount, l_Item.m_ItemDamage, l_Item.m_Enchantments:ToString(), l_Item.m_CustomName) .. ","
	end

	return l_Str
end

function AddToString(...)
	local l_Str = ""
	for i,v in ipairs(arg) do
		if l_Str == "" then
			l_Str = l_Str .. tostring(v) or ""
		else 
			l_Str = l_Str .. ":" .. tostring(v) or ""
		end
	end
	
	return l_Str
end

function CMDki(l_Split, l_Player)

	if l_Split[2] == nil then
		if l_Player then
			l_Player:SendMessage("keepInventory = "..tostring(gEnabled))
		else
			LOG("keepInventory = "..tostring(gEnabled))
		end
	end

	return true
end

function CMDkid(_, _)
	gEnabled = false
	return true
end
function CMDkie(_, _)
	gEnabled = true
	return true
end
function CMDkit(_, _)
	gEnabled = not gEnabled
	return true
end


-- https://gist.github.com/jaredallard/ddb152179831dd23b230 
function string:Split(delimiter)
	local result = { }
	local from  = 1
	local delim_from, delim_to = string.find( self, delimiter, from  )
	while delim_from do
		table.insert( result, string.sub( self, from , delim_from-1 ) )
		from  = delim_to + 1
		delim_from, delim_to = string.find( self, delimiter, from  )
	end
	table.insert( result, string.sub( self, from  ) )
	return result
end

function OnDisable()
    g_Storage.DB.DB:close()
    LOG(PLUGIN:GetName() .. " is shutting down...")
end